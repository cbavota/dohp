<?php
class DOHP_Search {
    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {
        if ( null == self::$instance ) {
            self::$instance = new DOHP_Search();
        }

        return self::$instance;
    }

    /**
     * Initializes the plugin by setting filters and administration functions.
     */
    private function __construct() {
		add_filter( 'template_include', array( $this, 'template_include' ) );

        add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ), 99 );
		add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ) );
    }

    public function wp_enqueue_scripts() {
	    global $wp_query;

	    if ( $wp_query->is_search && ! empty( $_GET['dohp'] ) ) {
			wp_enqueue_style( 'dohp_search', plugin_dir_URL( '' ) . 'dohp/css/search-results.css' );

			wp_enqueue_script( 'dohp-search', plugin_dir_URL( '' ) . 'dohp/js/search-results.js', array( 'jquery' ), '', true );
		}
    }

	public function template_include( $template ){
	    global $wp_query;

	    if ( $wp_query->is_search && ! empty( $_GET['dohp'] ) )
		    return dirname( __FILE__ ) . '/templates/search-results.php';

        return $template;
	}

	public function pre_get_posts( $query ) {
		if ( ! is_admin() && $query->is_main_query() ) {
			if ( $query->is_search && ! empty( $_GET['dohp'] ) ) {
				$query->set( 'post_type', 'oral_histories' );
			}
		}
	}
}
add_action( 'plugins_loaded', array( 'DOHP_Search', 'get_instance' ) );