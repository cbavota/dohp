( function( $ ) {
	$( '#additional-button' ).click(function() {
		var $el = $(this),
			$span = $el.find( 'span' );

		$el.blur();

		if ( $span.text() == $span.data( 'text-swap' ) ) {
			$span.text( $span.data( 'text-original' ) );
		} else {
			$span.data( 'text-original', $span.text() );
			$span.text( $span.data( 'text-swap' ) );
		}
		$( '.additional' ).slideToggle();
	} );
} )(jQuery);