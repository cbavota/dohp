<?php
/**
 * The template for displaying DOHP Search Results pages.
 *
 * @since 1.0.0
 */
get_header(); ?>

	<section id="primary" <?php bavotasan_primary_attr(); ?>>

		<?php if ( have_posts() ) : ?>
			<div class="container">
				<div class="row">
					<header id="archive-header" class="col-md-12">
						<h1 class="page-title page-header"><?php
							global $wp_query;
						    $num = $wp_query->found_posts;
							printf( '%1$s "%2$s"',
							    $num . __( ' search results for', 'matheson'),
							    get_search_query()
							);
						?></h1>
					</header>

					<div class="col-md-9 col-md-push-3">
						<?php while ( have_posts() ) : the_post(); ?>

							<?php
							$date_range[] = get_post_meta( get_the_ID(), 'recording_date', true );
							?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
								<?php $oh_meta_data = get_post_custom( get_the_ID() ); ?>

								<h1 class="entry-title taggedlink">
									<?php the_title(); ?>
									&nbsp;&nbsp;&bull;&nbsp;&nbsp;
									<?php echo $oh_meta_data['recording_date'][0]; ?>
								</h1>

							    <div class="entry-content description clearfix">
								    <br />
								    <strong>Bio:</strong>
								    <p><?php echo $oh_meta_data['narrator_bio'][0]; ?></p>

									<p>
									    <strong>Language of Interview: </strong>
										<?php echo ucfirst( $oh_meta_data['recording_language'][0] ); ?>
									</p>

									<p>
										<?php
										$mp3 = wp_get_attachment_metadata( absint( $oh_meta_data['attachments_mp3_id'][0] ) );
										?>
									    <strong>Length of Interview: </strong>
										<?php echo gmdate("H:i:s", $mp3['length'] ); ?>
									</p>

									<p class="more-link-p"><a class="btn btn-default" href="<?php the_permalink(); ?>"><?php _e( 'View', 'dohp' ); ?></a></p>
							    </div><!-- .entry-content -->
							</article><!-- #post-<?php the_ID(); ?> -->

						<?php endwhile; ?>
					</div>

					<div class="search-sidebar col-md-3 col-md-pull-9">
						<h4>Refine by</h4>
						<?php
						if ( 1 < count( $date_range ) ) {
							echo '<strong>Date</strong><ul>';
							foreach ( $date_range as $date ) {
								$the_date = date_parse( $date );
								echo '<li><label><input type="checkbox" name="date" value="' . $the_date['year'] . '"> ' . $the_date['year'] . '</label></li>';
							}
							echo '</ul>';
						}
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section><!-- #primary.c8 -->

<?php get_footer(); ?>