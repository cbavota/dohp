<?php
/**
 * The Template for displaying all oral histories.
 *
 * @since 1.0.0
 */
get_header(); ?>

	<div class="container">
		<div class="row">
			<div id="primary" class="col-md-12">
				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
						<?php
						$oh_meta_data = get_post_custom( get_the_ID() );
						?>

						<h1 class="entry-title page-header taggedlink">
							<?php the_title(); ?>
							&nbsp;&nbsp;&bull;&nbsp;&nbsp;
							<?php echo $oh_meta_data['recording_date'][0]; ?>
						</h1>

					    <div class="entry-content description clearfix">
						    <br />
						    <?php echo do_shortcode( '[audio src="' . esc_url( wp_get_attachment_url( absint( $oh_meta_data['attachments_mp3_id'][0] ) ) ) . '"]' ); ?>

						    <br />
						    <strong>Bio:</strong>
						    <p><?php echo $oh_meta_data['narrator_bio'][0]; ?></p>

							<br />
						    <p>
							    <strong>Date of Birth: </strong>
								<?php echo $oh_meta_data['narrator_birthdate_day'][0]; ?>/<?php echo $oh_meta_data['narrator_birthdate_month'][0]; ?>/<?php echo $oh_meta_data['narrator_birthdate_year'][0]; ?>
						    </p>

						    <p>
							    <strong>Place of Birth: </strong>
								<?php echo $oh_meta_data['narrator_birthcity'][0]; ?>, <?php echo $oh_meta_data['narrator_birthcountry'][0]; ?>
							</p>

							<p>
							    <strong>Gender: </strong>
								<?php echo ucfirst( $oh_meta_data['narrator_gender'][0] ); ?>
							</p>

							<p>
							    <strong>Language of Interview: </strong>
								<?php echo ucfirst( $oh_meta_data['recording_language'][0] ); ?>
							</p>

							<p>
								<?php
								$mp3 = wp_get_attachment_metadata( absint( $oh_meta_data['attachments_mp3_id'][0] ) );
								?>
							    <strong>Length of Interview: </strong>
								<?php echo gmdate("H:i:s", $mp3['length'] ); ?>
							</p>

							<br />
							<p><a href="#" id="additional-button" class="btn btn-default">Additional Information <span data-text-swap="-">+</span></a></p>

							<div class="additional">
							    <br />
							    <p>
								    <strong>Interviewer: </strong>
									<?php echo $oh_meta_data['interviewer_first_name'][0]; ?> <?php echo $oh_meta_data['interviewer_lastt_name'][0]; ?>
							    </p>

							    <p>
								    <strong>Date of Birth: </strong>
									<?php echo $oh_meta_data['interviewer_birthdate_day'][0]; ?>/<?php echo $oh_meta_data['interviewer_birthdate_month'][0]; ?>/<?php echo $oh_meta_data['interviewer_birthdate_year'][0]; ?>
							    </p>

							    <p>
								    <strong>Place of Birth: </strong>
									<?php echo $oh_meta_data['interviewer_birthcity'][0]; ?>, <?php echo $oh_meta_data['interviewer_birthcountry'][0]; ?>
								</p>

								<p>
								    <strong>Gender: </strong>
									<?php echo ucfirst( $oh_meta_data['interviewer_gender'][0] ); ?>
								</p>

							    <br />
							    <strong>Bio:</strong>
							    <p><?php echo $oh_meta_data['interviewer_bio'][0]; ?></p>

							    <br />
							    <strong>Reflection:</strong>
							    <p><?php echo $oh_meta_data['interview_reflection'][0]; ?></p>
								<br />

							    <p>
								    <strong>Relationship to narrator: </strong>
									<?php echo $oh_meta_data['recording_relationship'][0]; ?>
								</p>

							    <p>
								    <strong>Location: </strong>
									<?php echo $oh_meta_data['recording_address'][0]; ?>
								</p>


							</div>

					    </div><!-- .entry-content -->

					</article><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; // end of the loop. ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>