<?php
class DOHP_Single_Template {
    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {
        if ( null == self::$instance ) {
            self::$instance = new DOHP_Single_Template();
        }

        return self::$instance;
    }

    /**
     * Add needed functionality for new custom post type.
     */
    private function __construct() {
        // Filter the single template for oral history post type
		add_filter( 'single_template', array( $this, 'single_template' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );
    }

	public function wp_enqueue_scripts() {
		if ( is_single() && 'oral_histories' == get_post_type() ) {
			wp_enqueue_style( 'oral_histories_single', plugin_dir_url( '' ) . 'dohp/css/oral-histories-single.css' );
			wp_enqueue_script( 'oral_histories_single', plugin_dir_url( '' ) . 'dohp/js/oral-histories-single.js', array( 'jquery' ), '', true );
		}
	}

	public function single_template( $single ) {
	    global $wp_query, $post;

		/* Checks for single template by post type */
		if ( 'oral_histories' == $post->post_type ) {
		    if ( file_exists( plugin_dir_path(__FILE__) . 'templates/single-oral-histories.php' ) )
		        return plugin_dir_path(__FILE__) . 'templates/single-oral-histories.php';
		}

		return $single;
	}
}
add_action( 'plugins_loaded', array( 'DOHP_Single_Template', 'get_instance' ) );
