<?php
class DOHP_Submissions {
    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {
        if ( null == self::$instance ) {
            self::$instance = new DOHP_Submissions();
        }

        return self::$instance;
    }

    /**
     * Initializes the plugin by setting filters and administration functions.
     */
    private function __construct() {
        //add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

		add_shortcode( 'submissions', array( $this, 'submissions' ) );
    }

	public function wp_enqueue_scripts() {
		global $post;
		if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'submissions') ) {}
	}

	public function submissions() {
		$current_user = get_current_user_id();
		if ( $current_user ) {
			$args = array(
				'post_status' => 'any',
				'post_type' => 'oral_histories',
			);

			if ( ! current_user_can( 'manage_options' ) ) {
				$args['author'] = $current_user;
			}

			$oh_query = new WP_Query( $args );

			// display results from $author_query
			if ( $oh_query->have_posts() ) : ?>
				<table class="table">
					<tr>
						<th>Name</th><th>Author</th><th>Date</th><th>Status</th>
					</tr>
				<?php while ( $oh_query->have_posts() ) : $oh_query->the_post();
					$the_id = get_the_ID();
					$anchor_link = ( 'draft' == get_post_status() || current_user_can( 'manage_options' ) ) ? '<a href="' . wp_nonce_url( home_url( '/submissions/submission-form?id=' . $the_id  ), 'dohp_edit', 'dohp_edit' ) . '">' . get_the_title() . '</a>' : get_the_title();
					$view = ( 'publish' == get_post_status() ) ? ' <a href="' . esc_url( get_the_permalink() ) . '">(view)</a>' : '';
				?>
					<tr>
						<td><?php echo $anchor_link; ?></td>
						<td><?php echo get_the_author(); ?></td>
						<td><?php echo get_the_date(); ?></td>
						<td><?php echo get_post_status(); ?><?php echo $view; ?></td>
					</tr>
				<?php endwhile; ?>
				</table>
			<?php else : ?>
				<p>No submissions</p>
			<?php endif; ?>
			<a class="btn btn-warning btn-lg" href="<?php echo esc_url( home_url( '/submissions/submission-form' ) ); ?>"><?php _e( 'Add New', 'dohp' ); ?></a>
			<?php
			wp_reset_postdata();
		} else {
			echo '<p>No submissions</p>';
		}
	}
}
add_action( 'plugins_loaded', array( 'DOHP_Submissions', 'get_instance' ) );