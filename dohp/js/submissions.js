( function( $ ){
	$.fn.textareaCounter = function(options) {
		// setting the defaults
		// $("textarea").textareaCounter({ limit: 100 });
		var defaults = {
			limit: 100
		};
		var options = $.extend(defaults, options);

		// and the plugin begins
		return this.each(function() {
			var $obj, $counter, text, wordcount, limited;

			$obj = $(this);
			$obj.wrap( '<div class="counter-container"></div>' );
			$obj.after( '<span style="font-size: 11px; clear: both; margin-top: 3px; display: block; text-align:right;" class="counter-text">Max. ' + options.limit + ' words</span>' );

			$obj.keyup( function() {
			    $counter = $obj.parent().find( '.counter-text' );
			    text = $obj.val();
		    	wordcount = ( '' === text ) ? 0 : $.trim( text ).split( ' '  ).length;

			    if ( wordcount > options.limit ) {
			        $counter.html( '<span style="color: #DD0000;">0 words left</span>' );
					limited = $.trim( text ).split( ' ' , options.limit );
					limited = limited.join( ' ' );
					$obj.val( limited );
			    } else {
			        $counter.html( ( options.limit - wordcount ) + ' words left' );
			    }
			});
		});
	};
} )(jQuery);

( function( $ ) {
	var	$sidebar = $( '.submissions-sidebar' ),
		$slides = $( '.submissions-slides' );

	$sidebar.find( 'li' ).click( function() {
		var $el = $(this),
			page_move = $el.data( 'page' ),
			page_id = $el.data( 'id' ),
			last_page = $el.prev().data( 'id' ),
			$slide_check = $slides.find( '#' + last_page ),
			page_check;

		$sidebar.find( 'li' ).removeClass( 'active' );
		$el.addClass( 'active' );

		$slide_check.addClass( 'viewed' );
		$slides.find( '.slide' ).eq(0).css( 'margin-left', '-' + page_move + '%' );

		page_check = false;
		$slide_check.find( 'input, select, textarea' ).not( '.optional' ).each( function() {
			if ( '' == $(this).val() || null == $(this).val() ) {
				page_check = true;
				$(this).parents( '.form-group' ).addClass( 'has-error' );
			} else {
				$(this).parents( '.form-group' ).removeClass( 'has-error' );
			}
		} );

		if ( page_check ) {
			$sidebar.find( '.' + last_page ).removeClass( 'complete' );
		} else {
			$sidebar.find( '.' + last_page ).addClass( 'complete' );
		}

		if ( page_check ) {
			$slide_check.find( '.message-box' ).html( '<p class="alert alert-danger">You have not completed the ' + last_page + ' page</p>' );
		} else {
			$slide_check.find( '.message-box' ).html( '<p class="alert alert-success">You have completed the ' + last_page + ' page</p>' );
		}

		/*page_check = true;
		$( '#submit' ).find( '#validate-submission p' ).each( function() {
			if ( $(this).hasClass( 'alert-danger' ) ) {
				page_check = false;
				console.log( 'false' );
			}
		} );

		if ( page_check ) {
			$( '#submit-button' ).html( '<button type="submit" class="btn btn-success">Submit</button>' );
		}*/
	} );

	$slides
		.find( '.next-slide' ).click( function(e) {
			e.preventDefault();
			$sidebar.find( 'li.active' ).next().trigger( 'click' );
		} )
		.end()
		.find( '.previous-slide' ).click( function(e) {
			e.preventDefault();
			$sidebar.find( 'li.active' ).prev().trigger( 'click' );
		} )
		.end()
		.find( '.datepicker' ).datepicker({
		    showButtonPanel: true,
		    dateFormat: 'mm/dd/yy',
		    beforeShow: function(){
		           $( '.ui-datepicker' ).css( 'font-size', 12 );
		    }
		} )
		.end()
		.find( '.limit-textarea-less' ).textareaCounter( {
			limit: 25
		} )
		.end()
		.find( '.limit-textarea' ).textareaCounter( {
			limit: 100
		} )
		.end()
		.find( '.limit-textarea-more' ).textareaCounter( {
			limit: 200
		} )
		.end()
		.find( 'input, select, textarea' ).change( function() {
			if ( $(this).val() ) {
				$(this).parents( '.form-group' ).removeClass( 'has-error' );
			}
		} );

	/* Autosave functionality */
	var dohp_autosave_submission = function() {
		console.log( 'autosaving' );

		var spinner = $( '.submissions-spinner' ),
			nonce = $( '#dohp_nonce' ).val(),
			data = {
				action: 'dohp_autosave_submission',
				post_id: $slides.data( 'post-id' ),
				nonce: nonce,
				form: $( '.submissions-slides' ).serialize(),
			};

		spinner.show();
		$.post( DOHP.ajaxurl, data, function( response ) {
			spinner.hide();
			if ( response.success ) {
				console.log( response.data );
				$slides.removeData( 'post-id' );
				$slides.attr( 'data-post-id', response.post_id );
			} else {
				console.log( response.data );
			}
		}, 'json' );
	};

	$( '#save-draft' ).click( function(e) {
		e.preventDefault;
		dohp_autosave_submission();
		/* Remove comments below to turn on autosave */
		//setInterval( dohp_autosave_submission, 1000 * 60 * 1 );
	} );

	/* File upload */
    $( '#attachments' ).find( '.fileinput-button input' ).change( function() {
	    var	file = this.files[0],
	    	data = new FormData(),
	    	$parent = $(this).parents( '.form-group' ),
	    	$progress = $parent.find( '.progress' ),
	    	$filename = $parent.find( '.file-name' ),
	    	input_id = $(this).prop( 'id' ),
	    	ext = $(this).val().split('.').pop(),
	    	file_type = $(this).data( 'check' );

	    if ( file_type != ext ) {
	    	alert( 'Please select the proper file type.' );
	    	return;
	    }

    	data.append( 'nonce', $( '#dohp_nonce' ).val() );
		data.append( 'file', file );
    	data.append( 'action', 'dohp_file_upload' );
		data.append( 'post_id', $slides.data( 'post-id' ) );

		$progress.show();

		$.ajax( {
			url : DOHP.ajaxurl,
			type: 'POST',
			data: data,
			dataType: 'json',
			processData: false,
			contentType: false,
			xhr: function() {
				var xhr = new window.XMLHttpRequest();
				//Upload progress
				xhr.upload.addEventListener( 'progress', function(evt){
				  if ( evt.lengthComputable ) {
					var progress = parseInt( evt.loaded / evt.total * 100, 10 );
					$progress.find( '.progress-bar' ).css( 'width', progress + '%' );
				  }
				}, false);

				return xhr;
			},
			success: function( response ) {
				$progress.hide();
				console.log( response );
				$filename.html( 'Uploaded file: ' + response.filename );
				$( '#' + input_id + '_id' ).val( response.attachmentid );
			},
		} );
    } );
} )( jQuery );