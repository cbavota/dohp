( function( $ ) {
	$( '.search-sidebar' ).find( 'ul li input' ).change( function() {
		var url = window.location.href,
			search = $(location).attr( 'search' ),
			name = $(this).prop( 'name' ),
			pattern = new RegExp( '(' + name + '=).*?(&|$)' ),
			current_val = url.match( pattern ),
			current_val = ( current_val ) ? current_val[0].replace( current_val[1], '' ) : '',
			value = $(this).val(),
			value = ( -1 == current_val.indexOf( value ) ) ? value : current_val;
		    new_val = ( current_val && current_val != value && -1 == current_val.indexOf( value ) ) ? current_val + ',' + value : value;

	    if ( 0 <= url.search( pattern ) ) {
	        url = url.replace( pattern, '$1' + new_val + '$2' );
	    } else {
	        url = url + ( url.indexOf( '?' ) > 0 ? '&' : '?' ) + name + '=' + new_val;
	    }

		window.location.href = url;
	} );
} )(jQuery);