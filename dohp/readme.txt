=== Dawson Oral History Project ===
Contributors: tinkerpriest
Tags: oral history
Text Domain: dohp
Domain Path: /languages
Requires at least: 3.5
Tested up to: 4.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds all the functionality for the Dawson Oral History Project.

== Description ==

Adds all the functionality for the Dawson Oral History Project.


== Installation ==

1. Unzip the dohp.zip file.
2. Upload the `dohp` folder to the `/wp-content/plugins/` directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==


== Screenshots ==


== Change Log ==


== Upgrade Notice ==