<?php
class DOHP_Submission_Form {
    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {
        if ( null == self::$instance ) {
            self::$instance = new DOHP_Submission_Form();
        }

        return self::$instance;
    }

    /**
     * Initializes the plugin by setting filters and administration functions.
     */
    private function __construct() {
        add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );
        add_action( 'wp_ajax_dohp_autosave_submission', array( $this, 'dohp_autosave_submission' ) );
        add_action( 'wp_ajax_dohp_file_upload', array( $this, 'dohp_file_upload' ) );

		add_shortcode( 'submission-form', array( $this, 'submission_form' ) );
    }

	public function wp_enqueue_scripts() {
		global $post;
		if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'submission-form') ) {
			wp_enqueue_style( 'dohp-submissions', plugin_dir_URL( '' ) . 'dohp/css/submissions.css' );
			wp_enqueue_style( 'jquery-ui-datepicker-style' , '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');

			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_enqueue_script( 'dohp-submissions', plugin_dir_URL( '' ) . 'dohp/js/submissions.js', array( 'jquery', 'jquery-ui-datepicker' ), '', true );
			wp_localize_script( 'dohp-submissions', 'DOHP', array( 'ajaxurl' => admin_url( 'admin-ajax.php'), 'pluginurl' => plugin_dir_URL( '') ) );
		}
	}

	/**
	 * @todo	Loop through data, validate and save
	 * @todo	Create validation array for specific fields
	 */
    public function dohp_autosave_submission() {
		if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'dohp_nonce' ) )
			return;

        parse_str( $_POST['form'], $form_data );
        $success = true;
        $status = ( $form_data['admin_status'] ) ? $form_data['admin_status'] : 'draft';
		$current_post_status = get_post_status( absint( $_POST['post_id'] ) );
		$post_author = $form_data['interviewer_user_id'];

        $my_post = array(
	        'ID' => $_POST['post_id'],
			'post_title' => $form_data['narrator_first_name'] . ' ' . $form_data['narrator_last_name'],
			'post_content' => 'This is my post.',
			'post_author' => $post_author,
			'post_status' => $status,
			'post_type' => 'oral_histories',
		);

		// Insert the post into the database
		$new_post_id = wp_insert_post( $my_post );

		// Create meta fields
		foreach ( $form_data as $key => $value ) {
			if ( ! empty( $value ) ) {
				update_post_meta( $new_post_id, $key, $value );
			}
		}

		// If an admin has reverted the status back to a draft, send an email to the interviewer with
		// the admin notes as a message.
		if ( 'pending' == $current_post_status && 'draft' == $status ) {
			$headers = 'From: DOHP <info@dohp.com>' . "\r\n";
			wp_mail( is_email( $form_data['interviewer_email'] ), 'Please review your submission', esc_attr( $form_data['admin_notes'] ), $headers );
		}

        echo json_encode( array(
        	'success' => $success,
        	'post_id' => $new_post_id,
        	'data' => $form_data
        ) );

        die();
    }

	/**
	 * @todo
	 */
    public function dohp_file_upload() {
		if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'dohp_nonce' ) )
			return;

		// These files need to be included as dependencies when on the front end.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		$file_info = media_handle_upload( 'file', $_POST['post_id'] );

		if ( is_wp_error( $file_info ) ) {
			echo $file_info['error'];
		} else {
			echo json_encode( array(
				'attachmentid' => $file_info,
				'filename' => basename ( get_attached_file( $file_info ) ),
			) );
		}

        die();
    }

	public function submission_form() {
		if ( ! empty( $_GET['dohp_edit'] ) && wp_verify_nonce( $_GET['dohp_edit'], 'dohp_edit' ) ) {
			$oh_id = ( empty( $_GET['id'] ) ) ? '': $_GET['id'];
			$oh_meta_data = get_post_custom( $oh_id );
		}
		//print_r($oh_meta_data);
		?>
		<div class="row">
			<div class="col-md-3">
				<?php $this->dohp_submissions_sidebar(); ?>
			</div>

			<div class="col-md-9">
			    <form class="submissions-slides" data-post-id="<?php echo esc_attr( $oh_id ); ?>">
					<?php
					wp_nonce_field( 'dohp_nonce', 'dohp_nonce' );
						$int_user_id = get_post_field( 'post_author', absint( $oh_id ) );
					?>
					<input type="hidden" name="interviewer_user_id" value="<?php echo esc_attr( $int_user_id ); ?>" />

					<?php
					/**
					 * Interviewer section
					 */
					?>
				    <div id="interviewer" class="viewed slide">
					    <div class="message-box"></div>
					    <div class="row">
							<div class="form-group col-md-4">
								<?php $this->dohp_input( 'text', 'interviewer_first_name', __( 'First Name', 'dohp' ), __( 'Provide your full first name as the interviewer of the oral history.', 'dohp' ), $oh_meta_data ); ?>
							</div>

							<div class="form-group col-md-4">
								<?php $this->dohp_input( 'text', 'interviewer_last_name', __( 'Last Name', 'dohp' ), __( 'Provide your full last name as the interviewer of the oral history.', 'dohp' ), $oh_meta_data ); ?>
							</div>

							<div class="form-group col-md-4">
								<?php $this->dohp_input( 'text', 'interviewer_last_name_at_birth', __( 'Last Name at Birth', 'dohp' ), __( 'Provide your full last name at birth.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_input( 'email', 'interviewer_email', __( 'Email', 'dohp' ), __( 'Provide an email address where you can be contacted.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

						<div class="form-group">
							<?php $this->dohp_textarea( '', '3', 'interviewer_address', __( 'Home Address', 'dohp' ), __( 'Provide your complete address using the space provided. (optional)', 'dohp' ), $oh_meta_data, true ); ?>
						</div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_input( 'tel', 'interviewer_phone', __( 'Phone Number', 'dohp' ), __( 'Provide your complete phone number, including area code. (optional)', 'dohp' ), $oh_meta_data, true ); ?>
							</div>
					    </div>

						<div class="form-group">
							<?php $this->dohp_textarea( 'limit-textarea', '8', 'interviewer_bio', __( 'Brief Bio', 'dohp' ), __( 'Provide a brief description of yourself.', 'dohp' ), $oh_meta_data ); ?>
						</div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php
								$choices = array(
									'' => '--',
									'male' => __( 'Male', 'dohp' ),
									'female' => __( 'Female', 'dohp' ),
									'neither' => __( 'Does not identify as male or female', 'dohp' ),
								);
								$this->dohp_select( 'interviewer_gender', __( 'Gender', 'dohp' ), __( 'Select your gender from the dropdown list.', 'dohp' ), $oh_meta_data, $choices ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_birthday_selector( 'interviewer_birthdate', __( 'Date of Birth', 'dohp' ), __( 'Provide your date of birth.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_input( 'text', 'interviewer_birthcity', __( 'City of Birth', 'dohp' ), __( 'Provide the name of the city in which you were born.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_input( 'text', 'interviewer_birthcountry', __( 'Country of Birth', 'dohp' ), __( 'Provide the name of the country in which you were born.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

						<button class="btn btn-primary next-slide">Next &rarr;</button>
				    </div>
				    <?php
					/**
					 * Narrator section
					 */
					?>
				    <div id="interviewee" class="slide">
					    <div class="message-box"></div>
					    <div class="row">
							<div class="form-group col-md-4">
								<?php $this->dohp_input( 'text', 'narrator_first_name', __( 'First Name', 'dohp' ), __( 'Provide the full first name of the narrator of the oral history.', 'dohp' ), $oh_meta_data ); ?>
							</div>

							<div class="form-group col-md-4">
								<?php $this->dohp_input( 'text', 'narrator_last_name', __( 'Last Name', 'dohp' ), __( 'Provide the full last name of the narrator of the oral history.', 'dohp' ), $oh_meta_data ); ?>
							</div>

							<div class="form-group col-md-4">
								<?php $this->dohp_input( 'text', 'narrator_last_name_at_birth', __( 'Last Name at Birth', 'dohp' ), __( 'Provide the full last name at birth of the narrator of the oral history.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

					    <fieldset id="narrator_contact_info">
							<div class="row">
								<div class="form-group col-md-8">
									<?php $this->dohp_input( 'email', 'narrator_email', __( 'Email', 'dohp' ), __( 'Provide an email address where the narrator can be contacted.', 'dohp' ), $oh_meta_data, true ); ?>
								</div>
						    </div>

							<div class="form-group">
								<?php $this->dohp_textarea( '', '3', 'narrator_address', __( 'Home Address', 'dohp' ), __( 'Provide the complete address of the narrator using the space provided. (optional)', 'dohp' ), $oh_meta_data, true ); ?>
							</div>

						    <div class="row">
								<div class="form-group col-md-8">
									<?php $this->dohp_input( 'tel', 'narrator_phone', __( 'Phone Number', 'dohp' ), __( 'Provide the complete phone number of the narrator, including area code.', 'dohp' ), $oh_meta_data, true ); ?>
								</div>
						    </div>
					    </fieldset>

						<div class="form-group">
							<?php $this->dohp_textarea( 'limit-textarea', '8', 'narrator_bio', __( 'Brief Bio', 'dohp' ), __( 'Provide a brief description of the narrator of the oral history.', 'dohp' ), $oh_meta_data ); ?>
						</div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php
								$choices = array(
									'' => '--',
									'male' => __( 'Male', 'dohp' ),
									'female' => __( 'Female', 'dohp' ),
									'neither' => __( 'Does not identify as male or female', 'dohp' ),
								);
								$this->dohp_select( 'narrator_gender', __( 'Gender', 'dohp' ), __( 'Select the gender of the narrator from the dropdown list.', 'dohp' ), $oh_meta_data, $choices ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_birthday_selector( 'narrator_birthdate', __( 'Date of Birth', 'dohp' ), __( 'Provide the narrator&rsquo;s date of birth.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_input( 'text', 'narrator_birthcity', __( 'City of Birth', 'dohp' ), __( 'Provide the name of the city in which the narrator was born.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_input( 'text', 'narrator_birthcountry', __( 'Country of Birth', 'dohp' ), __( 'Provide the name of the country in which the narrator was born.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

						<button class="btn btn-info previous-slide">&larr; Back</button>
						<button class="btn btn-primary next-slide">Next &rarr;</button>
				    </div>
				    <?php
					/**
					 * Recording section
					 */
					?>
				    <div id="recording" class="slide">
					    <div class="message-box"></div>
						<div class="form-group">
							<?php $this->dohp_textarea( 'limit-textarea-more', '8', 'recording_summary', __( 'Summary of oral history interview', 'dohp' ), __( 'Provide a complete summary of the oral history interview. What topics were discussed? Write in complete sentences.', 'dohp' ), $oh_meta_data ); ?>
						</div>

						<div class="form-group">
							<?php $this->dohp_textarea( '', '3', 'recording_address', __( 'Location of recording', 'dohp' ), __( 'Provide the address of the place where the oral history was recorded.', 'dohp' ), $oh_meta_data ); ?>
						</div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_date_input( 'recording_date', __( 'Date of recording', 'dohp' ), __( 'mm/dd/yyyy', 'dohp' ), __( 'Provide the date that the oral history was recorded.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php $this->dohp_input( 'text', 'recording_language', __( 'Language of recording', 'dohp' ), __( 'Indicate the language of the oral history.', 'dohp' ), $oh_meta_data ); ?>
							</div>
					    </div>

						<div class="form-group">
							<?php $this->dohp_textarea( 'limit-textarea-less', '3', 'recording_relationship', __( 'Relationship', 'dohp' ), __( 'In a brief sentence, explain how you know the narrator.', 'dohp' ), $oh_meta_data ); ?>
						</div>

					    <div class="row">
							<div class="form-group col-md-8">
								<?php
								$choices = array(
									'' => '--',
									'DOHP' => __( 'DOHP', 'dohp' ),
								);
								$this->dohp_select( 'recording_project', __( 'Project', 'dohp' ), __( 'From the dropdown list, select the project that the oral history recording is being donated to.', 'dohp' ), $oh_meta_data, $choices ); ?>
							</div>
					    </div>

						<button class="btn btn-info previous-slide">&larr; Back</button>
						<button class="btn btn-primary next-slide">Next &rarr;</button>
				    </div>
				    <?php
					/**
					 * Reflections section
					 */
					?>
				    <div id="reflections" class="slide">
					    <div class="message-box"></div>
						<div class="form-group">
							<?php $this->dohp_textarea( 'limit-textarea-more', '14', 'interview_reflection', __( 'Interview Reflection', 'dohp' ), __( 'In this section you should provide a brief discussion of your experiences conducting the oral history. How did it feel to conduct the oral history? How did you feel about your use of silence and open questions? What went wrong? What went right? What was present, but not on the audio? (i.e. smells, appearances, textures, events that occurred &rsquo;off-mic,&rsquo; temperature, body language, etc...). If you have already contributed a reflection to the class forum, you may copy and paste that reflection directly into this space.', 'dohp' ), $oh_meta_data, false, true ); ?>
						</div>

						<button class="btn btn-info previous-slide">&larr; Back</button>
						<button class="btn btn-primary next-slide">Next &rarr;</button>
				    </div>
				    <?php
					/**
					 * Attachments section
					 * @todo	Complete upload form with all required elements
					 * @todo	Add validation for each input
					 * @todo	Write check for either JPG or PDF file type
					 */
					?>
				    <div id="attachments" class="slide">
					    <div class="message-box"></div>
						<div class="form-group">
							<label for="attachments_wav"><?php _e( 'Oral history recording in .WAV format', 'dohp' ); ?> <span class="dohp-tip" data-tip="<?php _e( 'Attach the .wav version of the oral history recording.', 'dohp' ); ?>"></span></label>

						    <div class="file-name"><?php
								if ( ! empty( $oh_meta_data['attachments_wav_id'][0] ) ) {
							    	echo __( 'Uploaded file: ', 'dohp' ) . basename ( get_attached_file( $oh_meta_data['attachments_wav_id'][0] ) );
							    }
							?></div>
						    <p>
							    <span class="btn btn-success fileinput-button">
							        <i class="fa fa-plus"></i>
							        <span><?php _e( 'Select WAV file...', 'dohp' ); ?></span>
							        <input id="attachments_wav" name="attachments_wav" data-check="wav" type="file" />
							    </span>
						    </p>
						    <div class="progress">
						        <div class="progress-bar progress-bar-warning progress-bar-striped"></div>
						    </div>
						    <input type="hidden" id="attachments_wav_id" name="attachments_wav_id" value="<?php echo esc_attr( $oh_meta_data['attachments_wav_id'][0] ); ?>" />
						</div>

						<div class="form-group">
							<label for="attachments_mp3"><?php _e( 'Oral history recording in .MP3 format', 'dohp' ); ?> <span class="dohp-tip" data-tip="<?php _e( 'Attach the .mp3 version of the oral history recording.', 'dohp' ); ?>"></span></label>

						    <div class="file-name"><?php
								if ( ! empty( $oh_meta_data['attachments_mp3_id'][0] ) ) {
							    	echo __( 'Uploaded file: ', 'dohp' ) . basename ( get_attached_file( $oh_meta_data['attachments_mp3_id'][0] ) );
							    }
							?></div>
						    <p>
							    <span class="btn btn-success fileinput-button">
							        <i class="fa fa-plus"></i>
							        <span><?php _e( 'Select MP3 file...', 'dohp' ); ?></span>
							        <input id="attachments_mp3" name="attachments_mp3" data-check="mp3" type="file" />
							    </span>
						    </p>
						    <div class="progress">
						        <div class="progress-bar progress-bar-warning progress-bar-striped"></div>
						    </div>
						    <input type="hidden" id="attachments_mp3_id" name="attachments_mp3_id" value="<?php echo esc_attr( $oh_meta_data['attachments_mp3_id'][0] ); ?>" />
						</div>

						<div class="form-group">
							<label for="attachments_consent_form"><?php _e( 'Signed consent form in .PDF or .JPG', 'dohp' ); ?> <span class="dohp-tip" data-tip="<?php _e( 'Attach a scanned or photographed copy of the consent form in .pdf format.', 'dohp' ); ?>"></span></label>
							<input type="file" id="attachments_consent_form" name="attachments_consent_form" />
						</div>

						<div class="form-group">
							<label for="attachments_photos"><?php _e( 'Images', 'dohp' ); ?> <span class="dohp-tip" data-tip="<?php _e( 'Attach any photographs that you have of the narrator.', 'dohp' ); ?>"></span></label>
							<input type="file" id="attachments_photos" name="attachments_photos" />
						</div>

						<button class="btn btn-info previous-slide">&larr; Back</button>
						<button class="btn btn-primary next-slide">Next &rarr;</button>
				    </div>
				    <?php
					/**
					 * Indexing section
					 */
					?>
				    <div id="indexing" class="slide">
					    <div class="message-box"></div>

						<button class="btn btn-info previous-slide">&larr; Back</button>
				    </div>
				    <?php
					/**
					 * Admin section
					 */
					?>
				    <div id="admin" class="slide">
	    				<?php if ( current_user_can( 'manage_options' ) ) { ?>
						<div class="form-group">
							<?php $this->dohp_textarea( '', '15', 'admin_notes', __( 'Admin Notes', 'dohp' ), __( 'Add admin notes here. (optional)', 'dohp' ), $oh_meta_data, true ); ?>
						</div>

						<div class="row">
							<div class="form-group col-md-8">
								<?php
								$choices = array(
									'draft' => __( 'Draft', 'dohp' ),
									'pending' => __( 'Pending', 'dohp' ),
									'publish' => __( 'Publish', 'dohp' ),
								);

								$oh_meta_data['admin_status'][0] = get_post_status( absint( $oh_id ) );

								echo get_post_status( absint( $oh_id ) );

								$this->dohp_select( 'admin_status', __( 'Status', 'dohp' ), __( 'Set the status for the submission.', 'dohp' ), $oh_meta_data, $choices ); ?>
							</div>
					    </div>
						<?php } ?>
				    </div>
			    </form>
		    </div>
		</div>
		<?php
	}

	private function dohp_submissions_sidebar() {
		$save_text = __( 'Save Draft', 'dohp' );
		?>
		<ul class="submissions-sidebar">
			<li class="active interviewer" data-id="interviewer" data-page="0">Interviewer</li>
			<li class="interviewee" data-id="interviewee" data-page="100">Narrator</li>
			<li class="recording" data-id="recording" data-page="200">The Recording</li>
			<li class="reflections" data-id="reflections" data-page="300">Reflections</li>
			<li class="attachments" data-id="attachments" data-page="400">File Attachments</li>
			<li class="indexing" data-id="indexing" data-page="500">Indexing</li>
			<?php if ( current_user_can( 'manage_options' ) ) {
				$save_text = __( 'Save', 'dohp' );
				?>
				<li class="admin" data-id="admin" data-page="600">Admin</li>
			<?php } ?>
		</ul>
		<p><a href="#" id="save-draft" class="btn btn-warning btn-lg btn-block"><?php echo $save_text; ?></a></p>
		<span id="submit-button"></span>
		<p class="submissions-spinner alert alert-warning"><?php _e( 'Saving', 'dohp' ); ?> <i class="fa fa-refresh fa-spin pull-right"></i></p>
		<?php
	}

	private function dohp_label( $name, $placeholder, $tip, $wide = false ) {
		$wide = ( $wide ) ? ' wide' : '';
		?>
		<label for="<?php echo esc_attr( $name ); ?>"><?php echo $placeholder; ?> <span class="dohp-tip<?php echo esc_attr( $wide ); ?>" data-tip="<?php echo esc_attr( $tip ); ?>"></span></label>
		<?php
	}

	private function dohp_input( $type, $name, $placeholder, $tip, $values, $optional = false ) {
		$value = ( empty( $values ) ) ? '' : $values[$name][0];
		$optional = ( $optional ) ? ' optional' : '';
		$this->dohp_label( $name, $placeholder, $tip );
		?>
		<input type="<?php echo esc_attr( $type ); ?>" class="form-control<?php echo esc_attr( $optional ); ?>" id="<?php echo esc_attr( $name ); ?>" name="<?php echo esc_attr( $name ); ?>" placeholder="<?php echo esc_attr( $placeholder ); ?>" value="<?php echo esc_attr( $value ); ?>" />
		<?php
	}

	private function dohp_date_input( $name, $title, $placeholder, $tip, $values ) {
		$value = ( empty( $values ) ) ? '' : $values[$name][0];
		$this->dohp_label( $name, $title, $tip );
		?>
		<input type="text" class="form-control datepicker" id="<?php echo esc_attr( $name ); ?>" name="<?php echo esc_attr( $name ); ?>" placeholder="<?php _e( 'mm/dd/yyyy', 'dohp' ); ?>" value="<?php echo esc_attr( $value ); ?>" />
		<?php
	}

	private function dohp_textarea( $class, $rows, $name, $placeholder, $tip, $values, $optional = false, $wide = false ) {
		$value = ( empty( $values ) ) ? '' : $values[$name][0];
		$class = ( empty( $class ) ) ? '' : ' ' . $class;
		$optional = ( $optional ) ? ' optional' : '';
		$this->dohp_label( $name, $placeholder, $tip, $wide );
		?>
		<textarea rows="<?php echo esc_attr( $rows ); ?>" class="form-control<?php echo esc_attr( $optional ); echo esc_attr( $class ); ?>" id="<?php echo esc_attr( $name ); ?>" name="<?php echo esc_attr( $name ); ?>" placeholder="<?php echo esc_attr( $placeholder ); ?>"><?php echo esc_attr( $value ); ?></textarea>
		<?php
	}

	private function dohp_select( $name, $placeholder, $tip, $selected, $choices ) {
		$this->dohp_label( $name, $placeholder, $tip );
		?>
		<select id="<?php echo esc_attr( $name ); ?>" name="<?php echo esc_attr( $name ); ?>" class="form-control">
			<?php
			foreach( $choices as $value => $option ) {
				echo '<option value="' . esc_attr( $value ) . '"' . selected( $selected[$name][0], $value, false ) . '>' . esc_attr( $option ) . '</option>';
			}
			?>
		</select>
		<?php
	}

	private function dohp_birthday_selector( $name, $placeholder, $tip, $selected ) {
		$this->dohp_label( $name, $placeholder, $tip );
		?>
		<div class="row">
			<div class="col-md-4">
				<select id="<?php echo esc_attr( $name ); ?>_day" name="<?php echo esc_attr( $name ); ?>_day" class="form-control">
					<option value="" disabled="disabled" <?php if ( empty( $selected[$name . '_day'][0] ) ) { ?>selected="selected"<?php } ?>><?php _e( 'Day', 'dohp' ); ?></option>
					<?php
					for ( $x = 1; $x <= 31; $x++ ) {
						echo '<option value="' . $x . '"' . selected( $selected[$name . '_day'][0], $x, false ) . '>' . $x . '</option>';
					}
					?>
				</select>
			</div>

			<div class="col-md-4">
				<select id="<?php echo esc_attr( $name ); ?>_month" name="<?php echo esc_attr( $name ); ?>_month" class="form-control">
					<option value="" disabled="disabled" <?php if ( empty( $selected[$name . '_month'][0] ) ) { ?>selected="selected"<?php } ?>><?php _e( 'Month', 'dohp' ); ?></option>
					<?php
					$months = array(
						'01' => __( 'January', 'dohp' ),
						'02' => __( 'February', 'dohp' ),
						'03' => __( 'March', 'dohp' ),
						'04' => __( 'April', 'dohp' ),
						'05' => __( 'May', 'dohp' ),
						'06' => __( 'June', 'dohp' ),
						'07' => __( 'July', 'dohp' ),
						'08' => __( 'August', 'dohp' ),
						'09' => __( 'September', 'dohp' ),
						'10' => __( 'October', 'dohp' ),
						'11' => __( 'November', 'dohp' ),
						'12' => __( 'December', 'dohp' ),
					);
					foreach ( $months as $number => $title ) {
						echo '<option value="' . $number . '"' . selected( $selected[$name . '_month'][0], $number, false ) . '>' . $title . '</option>';
					}
					?>
				</select>
			</div>

			<div class="col-md-4">
				<select id="<?php echo esc_attr( $name ); ?>_year" name="<?php echo esc_attr( $name ); ?>_year" class="form-control">
					<option value="" disabled="disabled" <?php if ( empty( $selected[$name . '_year'][0] ) ) { ?>selected="selected"<?php } ?>><?php _e( 'Year', 'dohp' ); ?></option>
					<?php
					for ( $x = 1915; $x <= date( 'Y' ); $x++ ) {
						echo '<option value="' . $x . '"' . selected( $selected[$name . '_year'][0], $x, false ) . '>' . $x . '</option>';
					}
					?>
				</select>
			</div>
		</div>
		<?php
	}
}
add_action( 'plugins_loaded', array( 'DOHP_Submission_Form', 'get_instance' ) );