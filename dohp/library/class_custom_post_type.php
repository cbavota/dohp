<?php
class DOHP_Custom_Post_Type {
    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {
        if ( null == self::$instance ) {
            self::$instance = new DOHP_Custom_Post_Type();
        }

        return self::$instance;
    }

    /**
     * Add needed functionality for new custom post type.
     */
    private function __construct() {
        // Hook into the init action to create the CPT.
		add_action( 'init', array( $this, 'init' ) );
    }

	public function init() {
		$labels = array(
			'name'               => _x( 'Oral Histories', 'post type general name', 'dohp' ),
			'singular_name'      => _x( 'Oral History', 'post type singular name', 'dohp' ),
			'menu_name'          => _x( 'Oral Histories', 'admin menu', 'dohp' ),
			'name_admin_bar'     => _x( 'Oral History', 'add new on admin bar', 'dohp' ),
			'add_new'            => _x( 'Add New', 'book', 'dohp' ),
			'add_new_item'       => __( 'Add New Oral History', 'dohp' ),
			'new_item'           => __( 'New Oral History', 'dohp' ),
			'edit_item'          => __( 'Edit Oral History', 'dohp' ),
			'view_item'          => __( 'View Oral History', 'dohp' ),
			'all_items'          => __( 'All Oral Histories', 'dohp' ),
			'search_items'       => __( 'Search Oral Histories', 'dohp' ),
			'parent_item_colon'  => __( 'Parent Oral Histories:', 'dohp' ),
			'not_found'          => __( 'No books found.', 'dohp' ),
			'not_found_in_trash' => __( 'No books found in Trash.', 'dohp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'oral-histories' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' )
		);

		register_post_type( 'oral_histories', $args );
	}
}
add_action( 'plugins_loaded', array( 'DOHP_Custom_Post_Type', 'get_instance' ) );
