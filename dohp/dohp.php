<?php
/*
Plugin Name: DOHP
Plugin URI: http://bavotasan.com/dohp/
Description: Adds all the functionality for the Dawson Oral History Project.
Author: c.bavota
Version: 1.0.0
Author URI: http://bavotasan.com
Text Domain: dohp
Domain Path: /languages
License: GPL2
*/

/*  Copyright 2015  c.bavota  (email : cbavota@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Plugin version
if ( ! defined( 'DOHP_VERSION' ) ) {
	define( 'DOHP_VERSION', '0.1' );
}

if ( ! class_exists( 'DOHP_Plugin' ) ) {
    class DOHP_Plugin {
        /**
         * Construct the plugin object
         */
        public function __construct() {
            add_action( 'admin_init', array( $this, 'admin_init' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );
			add_filter( 'wp_nav_menu_items', array( $this, 'wp_nav_menu_items' ), 10, 2 );

            include( plugin_dir_path( __FILE__ ) . '/library/class_custom_post_type.php' );
            include( plugin_dir_path( __FILE__ ) . '/library/class_submissions.php' );
            include( plugin_dir_path( __FILE__ ) . '/library/class_submission_form.php' );
            include( plugin_dir_path( __FILE__ ) . '/library/class_oral_history_single_template.php' );
            include( plugin_dir_path( __FILE__ ) . '/library/class_search.php' );
        }

        public function admin_init() {
			// Set up the textdomain and languages folder
            load_plugin_textdomain( 'dohp', null, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

            if ( get_page_by_title( 'Submissions' ) == NULL ) {
				$this->create_pages( 'Submissions', '[submissions]' );
            }

            if ( get_page_by_title( 'Submission Form' ) == NULL ) {
				$this->create_pages( 'Submission Form', '[submission-form]', $this->get_id_by_slug( 'submissions' ) );
            }
        }

        public function wp_enqueue_scripts() {
			// Load Font Awesome icon set
			wp_enqueue_style( 'font_awesome', plugin_dir_url( __FILE__ ) . 'css/font-awesome.css', false, '4.3.0', 'all' );
        }

        private function create_pages( $page_name, $shortcode, $parent = null ) {
			$args = array(
				'post_title' => $page_name,
				'post_content' => $shortcode,
				'post_status' => 'publish',
				'post_author' => 1,
				'post_type' => 'page',
				'post_name' => $page_name
			);

			if ( $parent ) {
				$args['post_parent'] = absint( $parent );
			}

	        // Insert page into the database
			wp_insert_post( $args );
	    }

	    public function get_id_by_slug( $page_slug ) {
		    $page = get_page_by_path( $page_slug );
		    if ( $page ) {
		        return $page->ID;
		    } else {
		        return null;
		    }
		}

		public function dohp_search_form( $echo = true ) {
			$form = '<form role="search" method="get" class="search-form" action="' . home_url() . '">
				<label>
					<input type="search" class="search-field" placeholder="' . esc_attr_x( 'Search ?', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x( 'Search for:', 'label' ) . '" />
					<input type="hidden" name="dohp" value="true" />
				</label>
			</form>';

		    if ( $echo )
		        echo $form;
		    else
		        return $form;
		}

		public function wp_nav_menu_items( $items, $args ) {
		    if ( 'primary' == $args->theme_location )
		        $items .= '<li class="dohp-search">' . $this->dohp_search_form( false ) . '</li>';

		    return $items;
		}
    }
 }
$dohp_plugin = new DOHP_Plugin();